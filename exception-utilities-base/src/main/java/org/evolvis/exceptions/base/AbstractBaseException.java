package org.evolvis.exceptions.base;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;


public abstract class AbstractBaseException extends Exception{
	
	private String code;		
	
	public AbstractBaseException(String code) {
		super();
		
		this.code=code;
	}
	
	public AbstractBaseException(String code, Throwable cause) {
		super(cause);		
		this.code=code;
	}
	
	public AbstractBaseException(String code, String message) {
		super(message);		
		this.code=code;
	}
	
	public AbstractBaseException(String code, String message,Throwable cause) {
		super(message,cause);		
		this.code=code;
	}

	

	public String getErrorCode() {	
		return code;
	}
	
	@Override
	/**
	 * template method for creating a localized message based on error.
	 */
	public String getLocalizedMessage() {		
		List<String> candidates = getCodeCandidates();
		ResourceBundle resourceBundle;
		try{			
			resourceBundle = getResourceBundle();
		}
		catch(MissingResourceException e){
			return getErrorCode()+" (Message not available, the resource bundle was not found!)";
		}
		String message = null;
		for(String code:candidates){			
			try{				
				message = resourceBundle.getString(code);
				break;
			}catch(MissingResourceException e){
				;
			}
		}
		if(message==null||message.trim().isEmpty()){
			return getErrorCode()+" (Message not available.)";
		}
		return message;
	}

	/**
	 * template method for creating a localized details based on error and arguments.
	 */
	public String getLocalizedDetails() {	
		List<String> candidates = getCodeCandidates();
		ResourceBundle resourceBundle;
		try{			
			resourceBundle = getResourceBundle();
		}
		catch(MissingResourceException e){
			return getErrorCode()+" (Details not available, the resource bundle was not found!)\n Arguments:" + Arrays.toString(getArguments());
		}
		String pattern = null;
		for(String code:candidates){
			String detailsCode = code+"_details";
			try{
				
				pattern = resourceBundle.getString(detailsCode);
				break;
			}catch(MissingResourceException e){
				;
			}
		}
		if(pattern==null||pattern.trim().isEmpty()){
			return getErrorCode()+" (No detail message was configured for this error code).\n Arguments:" + Arrays.toString(getArguments());
		}
		
		Object[] arguments = getArguments();
		String details = MessageFormat.format(pattern, arguments);
		return details;
	}

	private List<String> getCodeCandidates(){
		String code = getErrorCode();
		List<String> candidates = new ArrayList<String>();
		int index=code.length();
		do{
			candidates.add(code.substring(0,index));
			index = code.lastIndexOf('.', index-1);
		}while(index>0);
		return candidates;
	}
	
	public Object[] getArguments() {

		return new Object[]{};
	}



	
	
	public ResourceBundle getResourceBundle() {
		return ResourceBundle.getBundle("messages",Locale.getDefault(),this.getClass().getClassLoader());
	}
	
	

}
