package org.evolvis.exceptions.base;

import org.evolvis.exceptions.base.AbstractBaseException;

import junit.framework.TestCase;

public class ExceptionTest extends TestCase {

   private class DomainSpecificException extends AbstractBaseException{

	public DomainSpecificException(String code) {
		super(code);
	}
	@Override
	public Object[] getArguments() {		
		return new Object[]{23,42};
	}
   }
	
	public void test_01() {
		String msg=null;
		String code=null;
		String details=null;
		try {
			throw new DomainSpecificException("special.code");
		} catch (AbstractBaseException e) {		
			code = e.getErrorCode();			
			msg = e.getLocalizedMessage();
			details = e.getLocalizedDetails();
		}
		assertEquals("special.code",code);
		assertEquals("Hier ist was sehr, sehr schlimmes geschehen.",msg);
		assertEquals("Gute Argumente wären zum Beispiel 23 oder 42.",details);
		
	}
	

	public void test_02() {
		String msg=null;
		String code=null;
		String details=null;
		try {
			throw new DomainSpecificException("special");
		} catch (AbstractBaseException e) {		
			code = e.getErrorCode();
			details = e.getLocalizedDetails();
			msg = e.getLocalizedMessage();
		}
		assertEquals("special",code);
		assertEquals("Hier bleiben wir lieber mal allgemein.",msg);
		assertEquals("Gute Argumente wären zum Beispiel 23 oder 42.",details);
		
	}
	
	public void test_03() {
		String msg=null;
		String code=null;
		String details=null;
		try {
			throw new DomainSpecificException("special.gibtsnich");
		} catch (AbstractBaseException e) {		
			code = e.getErrorCode();
			details = e.getLocalizedDetails();
			msg = e.getLocalizedMessage();
		}
		assertEquals("special.gibtsnich",code);
		assertEquals("Hier bleiben wir lieber mal allgemein.",msg);
		assertEquals("Gute Argumente wären zum Beispiel 23 oder 42.",details);
	}
	
	public void test_04() {
		String msg=null;
		String code=null;
		String details=null;
		try {
			throw new DomainSpecificException("mich.gibtsnich");
		} catch (AbstractBaseException e) {		
			code = e.getErrorCode();
			details = e.getLocalizedDetails();
			msg = e.getLocalizedMessage();
		}
		assertEquals("mich.gibtsnich",code);
		assertEquals("mich.gibtsnich (Message not available.)",msg);
		assertEquals("mich.gibtsnich (No detail message was configured for this error code).\n Arguments:[23, 42]",details);
	}
	
	
	
	
	

}
